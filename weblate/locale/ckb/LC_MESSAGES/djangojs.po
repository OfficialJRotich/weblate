# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Weblate 3.10\n"
"Report-Msgid-Bugs-To: weblate@lists.cihar.com\n"
"POT-Creation-Date: 2019-11-27 10:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: weblate/static/loader-bootstrap.js:188
msgid "Add to screenshot"
msgstr ""

#: weblate/static/loader-bootstrap.js:205
msgid "Error loading search results!"
msgstr ""

#: weblate/static/loader-bootstrap.js:207
msgid "No new matching source strings found."
msgstr ""

#: weblate/static/loader-bootstrap.js:330
msgid "Copy"
msgstr ""

#: weblate/static/loader-bootstrap.js:336
msgid "Copy and save"
msgstr ""

#: weblate/static/loader-bootstrap.js:389
#, javascript-format
msgid "Ctrl+M then %s"
msgstr ""

#: weblate/static/loader-bootstrap.js:408
#, javascript-format
msgid "The request for machine translation using %s has failed:"
msgstr ""

#: weblate/static/loader-bootstrap.js:421
msgid "The request for machine translation has failed:"
msgstr ""

#: weblate/static/loader-bootstrap.js:496
msgid "Sort this column"
msgstr ""

#: weblate/static/loader-bootstrap.js:673
msgid "Error while loading page:"
msgstr ""

#: weblate/static/loader-bootstrap.js:1027
#, javascript-format
msgid "Ctrl/Command+%s"
msgstr ""

#: weblate/static/loader-bootstrap.js:1152
msgid "There are some unsaved changes, are you sure you want to leave?"
msgstr ""

#: weblate/static/loader-bootstrap.js:1178
#: weblate/static/loader-bootstrap.js:1180
msgid "Sunday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1178
msgid "Monday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1178
msgid "Tuesday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1179
msgid "Wednesday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1179
msgid "Thursday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1179
msgid "Friday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1180
msgid "Saturday"
msgstr ""

#: weblate/static/loader-bootstrap.js:1183
#: weblate/static/loader-bootstrap.js:1190
msgctxt "Short (eg. three letter) name of day in week"
msgid "Sun"
msgstr ""

#: weblate/static/loader-bootstrap.js:1184
msgctxt "Short (eg. three letter) name of day in week"
msgid "Mon"
msgstr ""

#: weblate/static/loader-bootstrap.js:1185
msgctxt "Short (eg. three letter) name of day in week"
msgid "Tue"
msgstr ""

#: weblate/static/loader-bootstrap.js:1186
msgctxt "Short (eg. three letter) name of day in week"
msgid "Wed"
msgstr ""

#: weblate/static/loader-bootstrap.js:1187
msgctxt "Short (eg. three letter) name of day in week"
msgid "Thu"
msgstr ""

#: weblate/static/loader-bootstrap.js:1188
msgctxt "Short (eg. three letter) name of day in week"
msgid "Fri"
msgstr ""

#: weblate/static/loader-bootstrap.js:1189
msgctxt "Short (eg. three letter) name of day in week"
msgid "Sat"
msgstr ""

#: weblate/static/loader-bootstrap.js:1193
#: weblate/static/loader-bootstrap.js:1200
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Su"
msgstr ""

#: weblate/static/loader-bootstrap.js:1194
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Mo"
msgstr ""

#: weblate/static/loader-bootstrap.js:1195
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Tu"
msgstr ""

#: weblate/static/loader-bootstrap.js:1196
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "We"
msgstr ""

#: weblate/static/loader-bootstrap.js:1197
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Th"
msgstr ""

#: weblate/static/loader-bootstrap.js:1198
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Fr"
msgstr ""

#: weblate/static/loader-bootstrap.js:1199
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Sa"
msgstr ""

#: weblate/static/loader-bootstrap.js:1203
msgid "January"
msgstr ""

#: weblate/static/loader-bootstrap.js:1203
msgid "February"
msgstr ""

#: weblate/static/loader-bootstrap.js:1203
msgid "March"
msgstr ""

#: weblate/static/loader-bootstrap.js:1204
msgid "April"
msgstr ""

#: weblate/static/loader-bootstrap.js:1204
msgid "May"
msgstr ""

#: weblate/static/loader-bootstrap.js:1204
msgid "June"
msgstr ""

#: weblate/static/loader-bootstrap.js:1204
msgid "July"
msgstr ""

#: weblate/static/loader-bootstrap.js:1205
msgid "August"
msgstr ""

#: weblate/static/loader-bootstrap.js:1205
msgid "September"
msgstr ""

#: weblate/static/loader-bootstrap.js:1205
msgid "October"
msgstr ""

#: weblate/static/loader-bootstrap.js:1206
msgid "November"
msgstr ""

#: weblate/static/loader-bootstrap.js:1206
msgid "December"
msgstr ""

#: weblate/static/loader-bootstrap.js:1209
msgctxt "Short name of month"
msgid "Jan"
msgstr ""

#: weblate/static/loader-bootstrap.js:1210
msgctxt "Short name of month"
msgid "Feb"
msgstr ""

#: weblate/static/loader-bootstrap.js:1211
msgctxt "Short name of month"
msgid "Mar"
msgstr ""

#: weblate/static/loader-bootstrap.js:1212
msgctxt "Short name of month"
msgid "Apr"
msgstr ""

#: weblate/static/loader-bootstrap.js:1213
msgctxt "Short name of month"
msgid "May"
msgstr ""

#: weblate/static/loader-bootstrap.js:1214
msgctxt "Short name of month"
msgid "Jun"
msgstr ""

#: weblate/static/loader-bootstrap.js:1215
msgctxt "Short name of month"
msgid "Jul"
msgstr ""

#: weblate/static/loader-bootstrap.js:1216
msgctxt "Short name of month"
msgid "Aug"
msgstr ""

#: weblate/static/loader-bootstrap.js:1217
msgctxt "Short name of month"
msgid "Sep"
msgstr ""

#: weblate/static/loader-bootstrap.js:1218
msgctxt "Short name of month"
msgid "Oct"
msgstr ""

#: weblate/static/loader-bootstrap.js:1219
msgctxt "Short name of month"
msgid "Nov"
msgstr ""

#: weblate/static/loader-bootstrap.js:1220
msgctxt "Short name of month"
msgid "Dec"
msgstr ""

#: weblate/static/loader-bootstrap.js:1222
msgid "Today"
msgstr ""

#: weblate/static/loader-bootstrap.js:1223
msgid "Clear"
msgstr ""

#: weblate/static/loader-bootstrap.js:1248
#, javascript-format
msgid "Ctrl+I then %s"
msgstr ""

#: weblate/static/loader-bootstrap.js:1462
msgid "Copied"
msgstr ""

#: weblate/static/loader-bootstrap.js:1485
msgid "Search…"
msgstr ""

#: weblate/static/loader-bootstrap.js:1486
msgid "Available:"
msgstr ""

#: weblate/static/loader-bootstrap.js:1487
msgid "Chosen:"
msgstr ""
