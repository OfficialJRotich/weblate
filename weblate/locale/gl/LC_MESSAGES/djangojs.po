# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Iván Seoane <ivanrsm1997@gmail.com>, 2018, 2019.
# Miguel A. Bouzada <mbouzada@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Weblate 3.10\n"
"Report-Msgid-Bugs-To: weblate@lists.cihar.com\n"
"POT-Creation-Date: 2019-11-27 10:45+0000\n"
"PO-Revision-Date: 2019-04-18 16:07+0000\n"
"Last-Translator: Iván Seoane <ivanrsm1997@gmail.com>\n"
"Language-Team: Galician <https://hosted.weblate.org/projects/weblate/"
"javascript/gl/>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.6-dev\n"

#: weblate/static/loader-bootstrap.js:188
msgid "Add to screenshot"
msgstr "Engadir á captura da pantalla"

#: weblate/static/loader-bootstrap.js:205
msgid "Error loading search results!"
msgstr "Erro ao cargar os resultados da procura!"

#: weblate/static/loader-bootstrap.js:207
msgid "No new matching source strings found."
msgstr "Sen novas cadeas fontes que correspondan."

#: weblate/static/loader-bootstrap.js:330
msgid "Copy"
msgstr "Copiar"

#: weblate/static/loader-bootstrap.js:336
msgid "Copy and save"
msgstr "Copiar e gardar"

#: weblate/static/loader-bootstrap.js:389
#, javascript-format
msgid "Ctrl+M then %s"
msgstr "Ctrl+M e despois %s"

#: weblate/static/loader-bootstrap.js:408
#, javascript-format
msgid "The request for machine translation using %s has failed:"
msgstr "Fallou a solicitude á máquina de tradución automática empregando %s:"

#: weblate/static/loader-bootstrap.js:421
msgid "The request for machine translation has failed:"
msgstr "Fallou a solicitude á máquina de tradución automática:"

#: weblate/static/loader-bootstrap.js:496
msgid "Sort this column"
msgstr "Ordenar esta columna"

#: weblate/static/loader-bootstrap.js:673
msgid "Error while loading page:"
msgstr "Produciuse un erro ao cargar a páxina:"

#: weblate/static/loader-bootstrap.js:1027
#, javascript-format
msgid "Ctrl/Command+%s"
msgstr "Ctrl/Comando+%s"

#: weblate/static/loader-bootstrap.js:1152
msgid "There are some unsaved changes, are you sure you want to leave?"
msgstr "Existen mudanzas sen gardar, ten a certeza de querer saír?"

#: weblate/static/loader-bootstrap.js:1178
#: weblate/static/loader-bootstrap.js:1180
msgid "Sunday"
msgstr "Domingo"

#: weblate/static/loader-bootstrap.js:1178
msgid "Monday"
msgstr "Luns (Segunda feira)"

#: weblate/static/loader-bootstrap.js:1178
msgid "Tuesday"
msgstr "Martes (Tercera feira)"

#: weblate/static/loader-bootstrap.js:1179
msgid "Wednesday"
msgstr "Mércores (Cuarta feira)"

#: weblate/static/loader-bootstrap.js:1179
msgid "Thursday"
msgstr "Xoves (Quinta feira)"

#: weblate/static/loader-bootstrap.js:1179
msgid "Friday"
msgstr "Venres (Sexta feira)"

#: weblate/static/loader-bootstrap.js:1180
msgid "Saturday"
msgstr "Sábado"

#: weblate/static/loader-bootstrap.js:1183
#: weblate/static/loader-bootstrap.js:1190
msgctxt "Short (eg. three letter) name of day in week"
msgid "Sun"
msgstr "Dom"

#: weblate/static/loader-bootstrap.js:1184
msgctxt "Short (eg. three letter) name of day in week"
msgid "Mon"
msgstr "Lun (Seg)"

#: weblate/static/loader-bootstrap.js:1185
msgctxt "Short (eg. three letter) name of day in week"
msgid "Tue"
msgstr "Mar (Ter)"

#: weblate/static/loader-bootstrap.js:1186
msgctxt "Short (eg. three letter) name of day in week"
msgid "Wed"
msgstr "Mér (Cua)"

#: weblate/static/loader-bootstrap.js:1187
msgctxt "Short (eg. three letter) name of day in week"
msgid "Thu"
msgstr "Xov (Qui)"

#: weblate/static/loader-bootstrap.js:1188
msgctxt "Short (eg. three letter) name of day in week"
msgid "Fri"
msgstr "Ven (Sex)"

#: weblate/static/loader-bootstrap.js:1189
msgctxt "Short (eg. three letter) name of day in week"
msgid "Sat"
msgstr "Sáb"

#: weblate/static/loader-bootstrap.js:1193
#: weblate/static/loader-bootstrap.js:1200
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Su"
msgstr "Do"

#: weblate/static/loader-bootstrap.js:1194
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Mo"
msgstr "Lu (2ª)"

#: weblate/static/loader-bootstrap.js:1195
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Tu"
msgstr "Ma (3ª)"

#: weblate/static/loader-bootstrap.js:1196
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "We"
msgstr "Mé (4ª)"

#: weblate/static/loader-bootstrap.js:1197
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Th"
msgstr "Xo (5ª)"

#: weblate/static/loader-bootstrap.js:1198
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Fr"
msgstr "Ve (6ª)"

#: weblate/static/loader-bootstrap.js:1199
msgctxt "Minimal (eg. two letter) name of day in week"
msgid "Sa"
msgstr "Sá"

#: weblate/static/loader-bootstrap.js:1203
msgid "January"
msgstr "Xaneiro"

#: weblate/static/loader-bootstrap.js:1203
msgid "February"
msgstr "Febreiro"

#: weblate/static/loader-bootstrap.js:1203
msgid "March"
msgstr "Marzo"

#: weblate/static/loader-bootstrap.js:1204
msgid "April"
msgstr "Abril"

#: weblate/static/loader-bootstrap.js:1204
msgid "May"
msgstr "Maio"

#: weblate/static/loader-bootstrap.js:1204
msgid "June"
msgstr "Xuño"

#: weblate/static/loader-bootstrap.js:1204
msgid "July"
msgstr "Xullo"

#: weblate/static/loader-bootstrap.js:1205
msgid "August"
msgstr "Agosto"

#: weblate/static/loader-bootstrap.js:1205
msgid "September"
msgstr "Setembro"

#: weblate/static/loader-bootstrap.js:1205
msgid "October"
msgstr "Outubro"

#: weblate/static/loader-bootstrap.js:1206
msgid "November"
msgstr "Novembro"

#: weblate/static/loader-bootstrap.js:1206
msgid "December"
msgstr "Decembro"

#: weblate/static/loader-bootstrap.js:1209
msgctxt "Short name of month"
msgid "Jan"
msgstr "Xan"

#: weblate/static/loader-bootstrap.js:1210
msgctxt "Short name of month"
msgid "Feb"
msgstr "Feb"

#: weblate/static/loader-bootstrap.js:1211
msgctxt "Short name of month"
msgid "Mar"
msgstr "Mar"

#: weblate/static/loader-bootstrap.js:1212
msgctxt "Short name of month"
msgid "Apr"
msgstr "Abr"

#: weblate/static/loader-bootstrap.js:1213
msgctxt "Short name of month"
msgid "May"
msgstr "Mai"

#: weblate/static/loader-bootstrap.js:1214
msgctxt "Short name of month"
msgid "Jun"
msgstr "Xuñ"

#: weblate/static/loader-bootstrap.js:1215
msgctxt "Short name of month"
msgid "Jul"
msgstr "Xul"

#: weblate/static/loader-bootstrap.js:1216
msgctxt "Short name of month"
msgid "Aug"
msgstr "Ago"

#: weblate/static/loader-bootstrap.js:1217
msgctxt "Short name of month"
msgid "Sep"
msgstr "Set"

#: weblate/static/loader-bootstrap.js:1218
msgctxt "Short name of month"
msgid "Oct"
msgstr "Out"

#: weblate/static/loader-bootstrap.js:1219
msgctxt "Short name of month"
msgid "Nov"
msgstr "Nov"

#: weblate/static/loader-bootstrap.js:1220
msgctxt "Short name of month"
msgid "Dec"
msgstr "Dec"

#: weblate/static/loader-bootstrap.js:1222
msgid "Today"
msgstr "Hoxe"

#: weblate/static/loader-bootstrap.js:1223
msgid "Clear"
msgstr "Limpar"

#: weblate/static/loader-bootstrap.js:1248
#, javascript-format
msgid "Ctrl+I then %s"
msgstr "Ctrl+I e despois %s"

#: weblate/static/loader-bootstrap.js:1462
msgid "Copied"
msgstr "Copiado"

#: weblate/static/loader-bootstrap.js:1485
msgid "Search…"
msgstr "Procurar…"

#: weblate/static/loader-bootstrap.js:1486
msgid "Available:"
msgstr "Dispoñíbel:"

#: weblate/static/loader-bootstrap.js:1487
msgid "Chosen:"
msgstr "Escollas:"

#, fuzzy
#~| msgctxt "Short name of month"
#~| msgid "Nov"
#~ msgid "Now"
#~ msgstr "Nov"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Fuzzy strings"
#~ msgstr "Cadeas revisábeis"

#~ msgid "Translated strings"
#~ msgstr "Cadeas traducidas"

#~ msgid "Ok"
#~ msgstr "Aceptar"

#~ msgid "AJAX request to load this content has failed!"
#~ msgstr "Fallou a solicitude AJAX ao cargar este contido!"

#~ msgid "Loading…"
#~ msgstr "Cargando…"

#~ msgid "Confirm resetting repository"
#~ msgstr "Confirmar o restabelecemento do repositorio"

#~ msgid "Resetting the repository will throw away all local changes!"
#~ msgstr "O reinicio do repositorio desfará todos os cambios locais!"

#~ msgid "Failed translation"
#~ msgstr "Fallou a tradución"

#~ msgid "Error details:"
#~ msgstr "Detalles do erro:"

#~ msgid "Translate using Apertium"
#~ msgstr "Traducir con Apertium"

#~ msgid "Translate using Microsoft Translator"
#~ msgstr "Traducir con Microsoft Translator"

#~ msgid "Translate using MyMemory"
#~ msgstr "Traducir con MyMemory"

#, fuzzy
#~ msgid "Loading..."
#~ msgstr "Cargando…"
